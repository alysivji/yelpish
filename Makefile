.PHONY: help

help: ## help
	@echo "Makefile for managing application:\n"
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

requirements: ## generate requirements.txt using piptoolsaa
	pip-compile --output-file=requirements.txt requirements.in

install:  ## install development dependencies
	pip install -r requirements_dev.txt

up: migrate-up  ## start development server
	python yelpish/manage.py runserver

migrate-up: ## run all migration
	python yelpish/manage.py migrate

migration:  ## create migration for app
	python yelpish/manage.py makemigrations -n "$(msg)" $(app)

migration-empty: ## create empty migration app="app" msg="msg"
	python yelpish/manage.py makemigrations --empty -n "$(msg)" $(app)

test:  ## run tests
	PYTHONPATH=./yelpish pytest

test-curr:  ## run test marked as current
	PYTHONPATH=./yelpish pytest -m 'current' --pdb

test-cov:  ## run tests with coverage
	PYTHONPATH=./yelpish pytest --cov ./yelpish $(args)
