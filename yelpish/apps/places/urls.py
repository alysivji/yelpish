from django.conf.urls import url

from .views import CuisinesListView, RestaurantsViewSet
from common.utilities import OptionalSlashRouter


router = OptionalSlashRouter()
router.register(r"api/v1/restaurants", RestaurantsViewSet, basename="restaurants")
urlpatterns = router.urls

urlpatterns += [
    url(r'api/v1/cuisines', CuisinesListView.as_view(), name="cuisines"),
]
