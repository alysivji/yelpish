from datetime import datetime
import logging

from django_fsm import TransitionNotAllowed
from django.shortcuts import get_object_or_404
from drf_yasg.utils import swagger_auto_schema
from rest_framework import status, viewsets
from rest_framework.decorators import action
from rest_framework.generics import ListAPIView
from rest_framework.response import Response

from .models import Cuisine, Restaurant, Review
from .serializers import (
    CuisineSerializer,
    RestaurantSerializer,
    RestaurantCreateItemSerializer,
    ReviewSerializer,
)


logger = logging.getLogger(__name__)


class RestaurantsViewSet(viewsets.ViewSet):
    queryset = Restaurant.objects

    @swagger_auto_schema(
        operation_description="List all restaurants",
        security=[],
        responses={200: RestaurantSerializer(many=True)},
    )
    def list(self, request):
        logger.info("recieved request")
        serializer = RestaurantSerializer(self.queryset, many=True)
        return Response(serializer.data)

    @swagger_auto_schema(
        operation_description="Retrieve restaurant instance details",
        security=[],
        responses={200: RestaurantSerializer(), 404: "Not found"},
    )
    def retrieve(self, request, pk=None):
        restaurant = get_object_or_404(self.queryset, uuid=pk)
        serializer = RestaurantSerializer(restaurant)
        return Response(serializer.data)

    @swagger_auto_schema(
        operation_description="Create new restaurant",
        security=[],
        request_body=RestaurantCreateItemSerializer(),
        responses={201: "Restaurant created successfully"},
    )
    def create(self, request):
        serializer = RestaurantCreateItemSerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            return Response(data={}, status=status.HTTP_201_CREATED)

    @swagger_auto_schema(
        operation_description="Update restaurant details",
        security=[],
        request_body=RestaurantCreateItemSerializer(),
        responses={200: "Restaurant updated successfully", 404: "Not found"},
    )
    def update(self, request, pk=None):
        restaurant = get_object_or_404(self.queryset, uuid=pk)
        serializer = RestaurantCreateItemSerializer(restaurant, data=request.data)
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            return Response(data={}, status=status.HTTP_200_OK)

    @swagger_auto_schema(
        operation_description="Delete restaurant details",
        security=[],
        responses={
            204: "Restaurant delete successfully",
            400: "Bad Request",
            404: "Not found",
        },
    )
    def destroy(self, request, pk=None):
        restaurant = get_object_or_404(self.queryset, uuid=pk)
        try:
            restaurant.attempt_delete()
        except TransitionNotAllowed:
            logger.error("Only inactive restaurants can be deleted")
            return Response(
                {"message": "Only inactive restaurants can be deleted"},
                status=status.HTTP_400_BAD_REQUEST,
            )
        restaurant.delete()
        return Response(data={}, status=status.HTTP_204_NO_CONTENT)

    @swagger_auto_schema(
        operation_description="Add review to restauarant",
        security=[],
        request_body=ReviewSerializer(),
        responses={201: "Review added successfully", 404: "Not found"},
    )
    @action(detail=True, methods=["post"])
    def review(self, request, pk=None):
        """Only had time to implement a rating system versus a full review platform"""
        restaurant = get_object_or_404(self.queryset, uuid=pk)
        serializer = ReviewSerializer(data=request.data)

        # not a fan of the nested logic here, but no time to fix
        if serializer.is_valid(raise_exception=True):
            review = Review()
            review.place = restaurant
            review.rating = serializer.validated_data["rating"]
            review.date_published = datetime.utcnow()
            review.save()
            return Response(data={}, status=status.HTTP_201_CREATED)


class CuisinesListView(ListAPIView):
    queryset = Cuisine.objects
    serializer_class = CuisineSerializer
