import uuid

from django.contrib.auth import get_user_model
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.db.models import Avg
from django_fsm import FSMField, transition
import jsonfield

from common.models import BaseModel

User = get_user_model()


class Place(BaseModel):
    id = models.AutoField(primary_key=True)
    uuid = models.UUIDField(default=uuid.uuid4, unique=True, null=False)
    # default=False might be better depending on the usecase
    # should all new places be active?
    is_active = FSMField(default=True, choices=[(True, "True"), (False, "False")])

    name = models.CharField(null=False, max_length=255)
    description = models.TextField(blank=True, null=False)

    address = models.CharField(null=False, max_length=255)  # hack for now
    url = models.URLField(max_length=200)

    # This is not done right, but don't have time to figure it out
    # Good enough for now, just need to figure out how this field works
    # Maybe it's a postgres versus sqlite thing
    # [{"day_of_week", "open", "close"}]
    hours_of_operation = jsonfield.JSONField(default=[])
    # {"phone", "fax"}
    contact_information = jsonfield.JSONField()

    @transition(is_active, source=["False"], target="True")
    def activate(self):
        pass

    @transition(is_active, source=["True"], target="False")
    def deactivate(self):
        pass

    @transition(is_active, source=["False"], target="False")
    def attempt_delete(self):
        pass

    @property
    def average_rating(self):
        reviews = self.reviews
        return list(reviews.aggregate(Avg("rating")).values())[0]


class Restaurant(Place):
    menu_url = models.URLField(max_length=200)
    cuisines = models.ManyToManyField("Cuisine", related_name="restaurants")


class Cuisine(BaseModel):
    id = models.AutoField(primary_key=True)
    name = models.CharField(null=False, max_length=30)  # aka short name
    description = models.CharField(null=False, blank=True, max_length=255)


class Review(BaseModel):
    """Holds information about user reviews of places

    This is the value-add of our website. Makes sense to use event sourcing here,
    but there isn't enough time.
    """

    class ReviewState:
        DRAFT = "draft"
        PUBLISHED = "published"
        FLAGGED = "flagged_for_review"

        CHOICES = [(DRAFT,) * 2, (PUBLISHED,) * 2, (FLAGGED,) * 2]

        INITIAL_STATE = "draft"

    id = models.AutoField(primary_key=True)
    uuid = models.UUIDField(default=uuid.uuid4, unique=True, null=False)
    place = models.ForeignKey(Place, on_delete=models.CASCADE, related_name="reviews")
    # don't have time to do users
    # user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="reviews")

    title = models.CharField(blank=True, null=False, max_length=255)
    content = models.TextField(blank=True, null=False)
    rating = models.PositiveIntegerField(
        default=10, validators=[MinValueValidator(1), MaxValueValidator(100)]
    )

    date_published = models.DateTimeField(null=False)
    review_state = FSMField(
        default=ReviewState.INITIAL_STATE, choices=ReviewState.CHOICES
    )
    # log changes [{"content": "", "rating": 1-100, "date": "ISO-8601"}]
    log = jsonfield.JSONField()

    # TODO
    # favourited review?
    # found review helpful?

    @transition(review_state, source=[ReviewState.DRAFT], target=ReviewState.PUBLISHED)
    def publish(self):
        pass
