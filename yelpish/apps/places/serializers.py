from rest_framework import serializers
from .models import Cuisine, Restaurant, Review


class CuisineSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cuisine
        fields = ["id", "name"]


class RestaurantSerializer(serializers.ModelSerializer):
    class Meta:
        model = Restaurant
        fields = [
            "uuid",
            "is_active",
            "name",
            "description",
            "address",
            "url",
            "hours_of_operation",
            "contact_information",
            "menu_url",
            "cuisines",
            "rating",
        ]

    cuisines = CuisineSerializer(many=True)
    rating = serializers.SerializerMethodField()

    def get_rating(self, restaurant):
        return restaurant.average_rating

    def create(self, validated_data):
        return Restaurant(**validated_data)

    def update(self, instance, validated_data):
        instance.name = validated_data.get("name", instance.email)
        instance.description = validated_data.get("description", instance.content)
        instance.address = validated_data.get("address", instance.created)
        instance.url = validated_data.get("url", instance.created)
        instance.menu_url = validated_data.get("menu_url", instance.created)
        return instance


class RestaurantCreateItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = Restaurant
        fields = [
            "name",
            "description",
            "address",
            "url",
            "hours_of_operation",
            "contact_information",
            "menu_url",
            "cuisines",
        ]


class ReviewSerializer(serializers.ModelSerializer):
    class Meta:
        model = Review
        fields = ["rating"]
