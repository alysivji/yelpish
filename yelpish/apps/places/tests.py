import pytest
from rest_framework.test import APIClient

from .factories import CuisineFactory, RestaurantFactory, ReviewFactory
from .models import Restaurant, Review


@pytest.fixture
def api_client():
    return APIClient()


class TestRestaurantModel:
    @pytest.mark.django_db
    @pytest.mark.parametrize(
        "active_flag, action, result",
        [("True", "deactivate", "False"), ("False", "activate", "True")],
    )
    def test_restaurant_toggle_is_active_state_machine(
        self, active_flag, action, result
    ):
        restaurant = RestaurantFactory(is_active=active_flag)

        action = getattr(restaurant, action)
        action()

        assert restaurant.is_active is result

    @pytest.mark.django_db
    def test_average_rating_property(self):
        restaurant = RestaurantFactory()
        ReviewFactory(place=restaurant, rating=10)
        ReviewFactory(place=restaurant, rating=8)
        ReviewFactory(place=restaurant, rating=6)

        avg = restaurant.average_rating

        assert avg == (10 + 8 + 6) / 3


class TestRestaraurantViewSet:
    @pytest.mark.django_db
    def test_get_restaurants(self, client):
        # Arrange
        RestaurantFactory.create_batch(size=10)

        # Act
        resp = client.get("/api/v1/restaurants")

        # Assert
        assert resp.status_code == 200

        items = resp.json()
        assert len(items) == 10

    @pytest.mark.django_db
    def test_get_single_restaurant(self, client):
        # Arrange
        r = RestaurantFactory(name="ROCKS Lakeview", is_active=False)
        food_type = CuisineFactory(name="American")
        r.cuisines.add(food_type)
        ReviewFactory(place=r, rating=10)

        # Act
        resp = client.get(f"/api/v1/restaurants/{r.uuid}")

        # Assert
        assert resp.status_code == 200

        item = resp.json()
        assert item["name"] == "ROCKS Lakeview"
        assert item["is_active"] is False
        assert len(item["cuisines"]) == 1
        assert item["cuisines"][0]["name"] == "American"
        assert item["rating"] == 10

    @pytest.mark.django_db
    def test_create_restaurant(self, client):
        # Arrange
        food_type = CuisineFactory(name="American")
        data = {
            "name": "ROCKS Lakeview",
            "description": "Whiskey. Beer. Burgers.",
            "address": "3463 N Broadway, Chicago, IL 60657",
            "url": "https://www.rockslakeview.com/",
            "hours_of_operation": [
                {
                    "day_of_week": "Monday",
                    "open_time": "11:00AM",
                    "close_time": "2:00AM",
                },
                {
                    "day_of_week": "Tuesday",
                    "open_time": "11:00AM",
                    "close_time": "2:00AM",
                },
            ],
            "contact_information": {"phone": "+1 555 234 3423"},
            "menu_url": "https://www.rockslakeview.com/",
            "cuisines": [food_type.id],
        }

        # Act
        resp = client.post("/api/v1/restaurants", data=data)

        # Assert
        assert resp.status_code == 201

        r = Restaurant.objects.first()
        assert r.name == "ROCKS Lakeview"
        assert r.is_active == "True"

        assert len(r.cuisines.all()) == 1
        cuisine = r.cuisines.first()
        assert cuisine.name == "American"

    @pytest.mark.django_db
    def test_update_restaurant(self, api_client):
        # Arrange
        r = RestaurantFactory(
            name="ROCKS Lakeview",
            description="Current Description goes here",
            is_active=False,
        )
        food_type = CuisineFactory(name="American")
        r.cuisines.add(food_type)

        data = {
            "name": "ROCKS Lakeview",
            "description": "Updated Description",
            "address": "3463 N Broadway, Chicago, IL 60657",
            "url": "https://www.rockslakeview.com/",
            "menu_url": "https://www.rockslakeview.com/",
            "cuisines": [food_type.id],
        }

        # Act
        resp = api_client.put(f"/api/v1/restaurants/{r.uuid}", data=data)

        # Assert
        assert resp.status_code == 200

        r = Restaurant.objects.first()
        assert r.name == "ROCKS Lakeview"
        assert r.description == "Updated Description"

    @pytest.mark.django_db
    def test_delete_restaurant(self, client):
        # Arrange
        r = RestaurantFactory(
            name="ROCKS Lakeview",
            description="Current Description goes here",
            is_active=False,
        )

        # Act
        resp = client.delete(f"/api/v1/restaurants/{r.uuid}")

        # Assert
        assert resp.status_code == 204
        assert len(Restaurant.objects.all()) == 0

    @pytest.mark.django_db
    def test_delete_restaurant__cannot_delete_inactive_restauarant(self, client):
        """Try to delete an inactive restaurant, get denied"""
        # Arrange
        r = RestaurantFactory(
            name="ROCKS Lakeview",
            description="Current Description goes here",
            is_active=True,
        )

        # Act
        resp = client.delete(f"/api/v1/restaurants/{r.uuid}")

        # Assert
        assert resp.status_code == 400
        assert len(Restaurant.objects.all()) == 1

    @pytest.mark.django_db
    def test_review_restaurant(self, client):
        # Arrange
        r = RestaurantFactory(
            name="ROCKS Lakeview",
            description="Current Description goes here",
            is_active=True,
        )

        # Act
        data = {"rating": 10}
        resp = client.post(f"/api/v1/restaurants/{r.uuid}/review", data=data)

        # Assert
        assert resp.status_code == 201
        assert len(Review.objects.all()) == 1

        review = Review.objects.first()
        assert review.rating == 10
        assert review.place.uuid == r.uuid
