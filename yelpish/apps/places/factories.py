from datetime import datetime
import factory

from .models import Cuisine, Restaurant, Review


class RestaurantFactory(factory.DjangoModelFactory):
    class Meta:
        model = Restaurant

    is_active = True

    name = "ROCKS Lakeview"
    description = "Whiskey. Beer. Burgers."

    address = "3463 N Broadway, Chicago, IL 60657"
    url = "https://www.rockslakeview.com/"


class CuisineFactory(factory.DjangoModelFactory):
    class Meta:
        model = Cuisine

    name = "Indian"


class ReviewFactory(factory.DjangoModelFactory):
    class Meta:
        model = Review

    place = factory.SubFactory(RestaurantFactory)
    rating = 5
    date_published = factory.LazyFunction(datetime.now)
