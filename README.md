# Yelp-ish

A Yelp clone.

#### Table of Contents

<!-- TOC -->

- [Instructions](#instructions)
  - [Setting up Environment](#setting-up-environment)
  - [Viewing API Documentation](#viewing-api-documentation)
  - [Run tests](#run-tests)
- [Problem Description](#problem-description)
- [Solution Description](#solution-description)
  - [Design](#design)
  - [Tests](#tests)
  - [Notes about time](#notes-about-time)
  - [Trade-offs](#trade-offs)
- [Contact Information](#contact-information)
  - [Other Projects](#other-projects)

<!-- /TOC -->

## Instructions

### Setting up Environment

1. Create a virtual environment for Python 3.7+
1. `make install`
1. `make up`
1. Go to http://localhost:8000/healthcheck/ to make service is up

Windows users: please use Windows Subsystem for Linux or Choclately to get Make working correctly.

### Viewing API Documentation

http://localhost:8000/docs

### Run tests

```console
make test
make test-cov
```

## Problem Description

Create a Yelp clone that allows:
- users to perform CRUD operations for Restaurants via REST API
- rate restaurants
- developers to easily extend to different business types / entities

## Solution Description

Created a backend users can interact with via REST calls.

### Design

#### Models

- used SQLite since this is an MVP
  - would prefer to use postgres when possible
- Created a base model called `Place` which represents all types of entities (Stores, Law Firms, Restaurants, etc)
  - contains information that is required for all places
- `Restaurant` model has a base class of `Place`
  - contains restaurant specific information
  - MVP uses Restaurant as Proof-of-Concept throughout
- `Cuisine` model keeps track of "food-type" the Restaurant serves
  - many-to-many relationship with Restaurant as this list will not be static
    - food preferences change (ramen was not a thing 10 years ago)
    - restaurants can have many different types of food (Cheesecake Factory)
- `Review` model keeps track of ratings
  - built out functionality for users to do reviews, but did not have time to implement
  - my reasoning here is that people come to Yelp for reviews and ratings, not just ratings
    - good to start thinking about the functionality of reviews and the various states they can be in (i.e. `DRAFT`, `PUBLISHED`, `FLAGGED_FOR_REVIEW`)
- used `django-fsm` to build out state machine workflows around whether a restaurant is active / inactive
  - taking time upfront to add explicit state machine transitions will enable us to reduce code and build an audit log

#### Views

- CRUD endpoints around the `Restaraunt` model
  - users can also `POST` a rating (1 - 100) to `/api/v1/restaraunts/<uuid>/review`
- used DRF Viewsets as a lot of the work is very boilerplate
  - can plug in authentication and permissions using a class-level variable
- DRF ModelSerializers reduce boilerplate
- added API documentation using `drf-yasg` which also helps document my code: http://localhost:8000/docs

### Tests

- spent some time upfront setting up Django to work with pytest
  - enabled me to iterate a lot faster
- wrote tests slightly after the code to make sure it worked as intended
  - versus checking manually with a tool like postman
- leveraged `factory-boy` to create test fixtures
- 93% test coverage with mostly end-to-end tests

### Notes about time

I took around 4-5 hours to get this solution to its current state; this does not count the time it took me to set up my project.

Add another 30-40 minutes for the README.

### Trade-offs

- did not get a chance to add filtering restaurants by rating
  - I spent too much time thinking about what makes up a Review versus focusing in on a rating
- did not implement a custom User model for authentication

## Contact Information

- [Resume](https://docs.google.com/document/d/1aLyNoVxSajOdSv6bsmAnjFuxtldUtOhvR5Gqacifyss/edit?usp=sharing)
- [GitHub](https://github.com/alysivji)

### Other Projects

- [finite-state-machine](https://github.com/alysivji/finite-state-machine) -- Lightweight, decorator-based Python implementation of a Finite State Machine
  - wanted to replicate the behavior of `django-fsm` for my other projects
  - Available for others to use: `pip install finite-state-machine`
- [Busy Beaver](https://github.com/busy-beaver-dev/busy-beaver) -- Chicago Python Community Engagement Slackbot
  - built with Flask
  - allowed me to practice Design Patterns in a real project to find out where and how its best to use them
  - this is an open-source project that has 10-15 contributors
    - learned about how to build a community from the ground up
- [Street Team](https://github.com/alysivji/street-team) -- app to allow users to upload event pictures for social media sharing
  - Django app
  - built for Chicago Python before COVID-19 hit
  - recently been playing around with the concepts of Event Sourcing to add an explicit audit trail to the database
